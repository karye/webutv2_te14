$(document).ready(function() {

    // Skapar alla variabler
    var xPlayer = 1, yPlayer = 1;

    // Ansluter till canvas och rit-gränssnittet
    var c = document.getElementById("labyrint");
    c.width = 600;
    c.height = 600;
    var ctx = c.getContext("2d");

    // Karta som ligger i en array
    var map = [
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        [1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1],
        [1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1],
        [1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1],
        [1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1],
        [1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        [1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1],
        [1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1],
        [1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1],
        [1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
        [1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1],
        [1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1],
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    ];

    // Här sätter upp spelet
    function run() {

        // Upprepa gameLoop 30 ggr/sek
        setInterval(gameLoop, 30);
    }

    // Här spelets motor - kod som körs 30 ggr/sek
    function gameLoop() {

        // Tömma ritytan
        ctx.clearRect(0, 0, c.width, c.height);

        // Rita kartan
        drawMap();

        // Rita spelaren
        drawPlayer();
    }

    // Rita ut kartan
    function drawMap() {
        ctx.fillStyle = '#000';
        for (var i = 0; i < map.length; i++) {
            for (var j = 0; j < map[i].length; j++) {
                if (map[j][i] == 1)
                    ctx.fillRect(i * 40, j * 40, 40, 40);
            }
        }
    }

    // Rita ut spelare
    function drawPlayer() {
        ctx.fillStyle = '#ffaaaa';
        ctx.fillRect(xPlayer * 40 + 10, yPlayer * 40 + 10, 20, 20);
    }

    // Här lyssnar vi tangenter så vi kan styra spelet
    $("#labyrint").keydown(function(event) {
        console.log(event.which);
        // Åt vänster = tangent 4 = 100
        if (event.which == 100 && map[yPlayer][xPlayer-1] == 0)
            xPlayer--;
        // Åt höger = tangent 6 = 102
        if (event.which == 102 && map[yPlayer][xPlayer+1] == 0)
            xPlayer++;
        // Uppåt = tangent 8 = 104
        if (event.which == 104 && map[yPlayer-1][xPlayer] == 0)
            yPlayer--
        // Nedåt = tangent 2 = 98
        if (event.which == 98 && map[yPlayer+1][xPlayer] == 0)
            yPlayer++;
    });

    run();
});
