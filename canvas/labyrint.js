// Skapar alla variabler
var c, ctx, xPlayer = 1, yPlayer = 1;

// Karta som ligger i en array
var map = [
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1],
    [1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1],
    [1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1],
    [1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1],
    [1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1],
    [1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1],
    [1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1],
    [1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1],
    [1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1],
    [1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
];

// Här sätter upp spelet
function run() {

    // Ansluter till canvas och rit-gränssnittet
    c = document.getElementById("labyrint");
    ctx = c.getContext("2d");

    // Upprepa gameLoop 30 ggr/sek
    setInterval(gameLoop, 30);
}

// Här spelets motor - kod som körs 30 ggr/sek
function gameLoop() {

    // Tömma ritytan
    ctx.clearRect(0, 0, c.width, c.height);

    // Rita kartan
    drawMap();

    // Rita spelaren
    drawPlayer();
}

// Rita ut kartan
function drawMap() {
    ctx.fillStyle = '#000';
    for (var i = 0; i < map.length; i++) {
        for (var j = 0; j < map[i].length; j++) {
            if (map[j][i] == 1)
                ctx.fillRect(i * 40, j * 40, 40, 40);
        }
    }
}

// Rita ut spelare
function drawPlayer() {
    ctx.fillStyle = '#ffaaaa';
    ctx.fillRect(xPlayer * 40 + 10, yPlayer * 40 + 10, 20, 20);
}

// Här lyssnar vi tangenter så vi kan styra spelet
function keyDown(e) {
    // Pil-höger
    if (e.keyCode == 39)
        xPlayer++;
}
