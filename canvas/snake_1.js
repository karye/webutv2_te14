// Allmäna variabler
var c, ctx;

// Figurens position
snakePartsX = new Array();
snakePartsY = new Array();

// Ormens storlek
var snakeRadius = 3;
var snakeLength = 50;

// Ormens hastighet
var snakeSpeed = 4;
var snakeSpeedX = snakeSpeed;
var snakeSpeedY = 0;

// Ormens startposition
snakePartsX[0] = 0;
snakePartsY[0] = snakeRadius;

function run() {
    c = document.getElementById("myCanvas");
    ctx = c.getContext("2d");

    // Upprepa var 30ms
    window.setInterval(gameLoop, 30);
}

// Spelmotorn
function gameLoop() {

    // Sudda canvas
    ctx.clearRect(0, 0, c.width, c.height);

    // Rita ut ormen
    ctx.beginPath();
    ctx.arc(snakePartsX[0], snakePartsY[0], snakeRadius, 0, 2 * Math.PI);
    ctx.fill();

    console.log(snakePartsX + " " + snakePartsY);
    updatePositions();
}

// Ändrar ormens position
function updatePositions() {
    snakePartsX.unshift(snakePartsX[0] + snakeSpeedX);
    snakePartsY.unshift(snakePartsY[0] + snakeSpeedY);
}

// Lyssna på tangentnedtryckningar
function keyDown(e) {
    switch(e.keyCode) {
        case 37: // Vänster

            break;

        case 39: // Höger

            break;

        case 38: // Upp

            break;

        case 40: // Ned

            break;
    }
}
